package suporte;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class web {
    @Test
    public static WebDriver createChrome() {


        System.setProperty("webdriver.chrome.driver", "C:\\Drivers\\chromedriver.exe");
        WebDriver navegador = new ChromeDriver();

        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        // Site à ser aberto
        navegador.get("http://automationpractice.com/index.php?");

        return navegador;


    }

}


